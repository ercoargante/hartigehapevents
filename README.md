# README #

### What is this repository for? ###

Uitwerking van het IDH5 proeftentamen "Hartige Hap Events".

TODO: commentaar in de broncode en javadoc zijn nog veelal in het Nederlands.


* Version 1.0.0.RELEASE
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Dit project is opgezet volgens de Maven projectstructuur, zodat het gemakkelijk in de ontwikkelstraat gehangen kan worden.
* Je kunt een maven project prima runnen vanaf de command line. mvn.bat moet dan wel in je zoekpad staan.
* In Netbeans, kies �File/Open Project� en ga naar de map <projectnaam>.
* In Eclipse, kies �Import �/Existing Maven projects�.
* In IntelliJ, kies �import project� en je selecteert dan het pom.xml file
* In de map libraryWorkedExample\src\main\resources bevindt zich het bestand Database_Setup_READ_FIRST.txt met aanwijzingen voor de setup van de database
* Voor extra informatie zie het document op Blackboard "HowTo_ProftaakInOntwikkelstraatHangen.docx".


### Who do I talk to? ###

* Repo owner
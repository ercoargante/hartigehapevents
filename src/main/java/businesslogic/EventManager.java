/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

import domain.Event;
import java.util.ArrayList;
import java.util.Date;



/**
 *
 * De evenementen manager houdt dingen bij die met evenementen te maken hebben.
 * Ook moet je via deze manager de evenementen zoeken en ophalen.
 *
 * @author Richard Lindhout
 * @version 1.0
 * */
public interface EventManager {
    
    /**
     * @param eventId
     *            - het ID van het evenement
     * @return Evenementsdatum - de datum waarop het evenement plaatsvind
     * 
     * */
    public Date getEventDate(int eventId);
    

    /**
     * @return Evenementen lijst
     * */
    public ArrayList<Event> getEvents();

    
    /**
     * 
     * @return int[] array met evenement ID's
     * */
    public int[] getEventIds();
    

  /**
   * @param eventId het ID van het evenement. Kan niet negatief zijn.
   * @return Evenement - kan een KookWorkshop of Demonstratie zijn.
   * */
    public Event getEventInfo(int eventId);
    

   /**
    * @return Evenementnaam - Naam van evenement bijv: "Kookworkshop groente koken"
    * @param eventId - ID van het evenement
    * */
    public String getEventName(int eventId);
    
}


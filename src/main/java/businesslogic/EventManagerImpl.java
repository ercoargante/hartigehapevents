package businesslogic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import domain.Demonstration;
import domain.Event;
import domain.Ingredient;
import domain.CookingWorkshop;

/**
 *
 * De evenementen manager houdt dingen bij die met evenementen te maken hebben.
 * Ook moet je via deze manager de evenementen zoeken en ophalen.
 *
 * @author Richard Lindhout
 * @version 1.0
 * */
public class EventManagerImpl implements EventManager {

    ArrayList<Event> events;

    /**
     *
     * Hier worden wat test items gemaakt waarmee de evenement manager getest worden.
     * Ook wordt de evenementen arrayList aangemaakt.
     * 
     * De data moet in de toekomst opgehaald worden van een database.
     * */
    public EventManagerImpl() {
        // empty constructor
        events = new ArrayList<>();

        // some hard coded date because we don't need persistent data
        Ingredient spaghetti = new Ingredient("Spaghetti", 180, "gram");
        Ingredient tomaten = new Ingredient("Tomaten", 5, "kg");

        ArrayList<Ingredient> ingredients1 = new ArrayList<>();
        ingredients1.add(spaghetti);
        ingredients1.add(tomaten);

        ArrayList<Ingredient> ingredients2 = new ArrayList<>();
        ingredients1.add(spaghetti);

        // current date
        Date date1 = new Date();

        // make different events
        Event evenement1 = new CookingWorkshop(1, "Kookworkshop italiaans",
                date1, 12, "Jan Montizaam", ingredients1);

        Event evenement2 = new CookingWorkshop(2, "Kookworkshop rara", date1,
                12, "Richard Lindhout", ingredients2);

        Event evenement3 = new Demonstration(3, date1, 20,
                "Richard Lindhout", "Demonstratie jaja rara", "Jan montziaam",
                "06 -  43424567");

        // add events to manager
        events.add(evenement1);
        events.add(evenement2);
        events.add(evenement3);
    }


    @Override
    public Date getEventDate(int eventId) {
        for (Event evenement : events) {
            if (evenement.getId() == eventId) {
                return evenement.getDate();
            }
        }
        return null;
    }


    @Override
    public ArrayList<Event> getEvents() {
        return events;
    }


    @Override
    public int[] getEventIds() {
        // We cant change an int[] Array
        // so we do need to set it directly

        // first make integer list
        List<Integer> list = new ArrayList<>();

        // add all ID's to the list
        for (Event evenement : events) {
            list.add(evenement.getId());
        }

        // insert list in the int[] array
        int[] ints = list.stream().mapToInt(i -> i).toArray();

        return ints;
    }


    @Override
    public Event getEventInfo(int eventId) {

        try {
            // Kan geen negatief nummer zijn, dus we doen een Exception
            if (eventId < 0) {
                throw new InvalidEventIDException(eventId);
            }

            for (Event event : events) {

                if (event.getId() == eventId) {
                    return event;
                }
            }

        } catch (InvalidEventIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public String getEventName(int eventId) {
        for (Event event : events) {
            if (event.getId() == eventId) {
                return event.getName();
            }
        }
        return null;
    }
}

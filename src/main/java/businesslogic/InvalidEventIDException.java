/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package businesslogic;

/**
 *
 * @author ppthgast
 */
public class InvalidEventIDException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int eventID;

    /**
     * Constructor
     *
     * @param eventID
     *            het foutieve event ID dat gebruikt werd
     */
    public InvalidEventIDException(int eventID) {
        this.eventID = eventID;
    }

    /**
     * Override voor de string representatie van de exception.
     *
     * @return string representatie inclusief event ID
     */
    @Override
    public String toString() {
        return "Er is een foutief event ID ingevoerd: " + eventID;
    }
}

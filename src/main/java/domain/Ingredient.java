/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author ppthgast
 *
 * @version 1.0
 *
 *          Representeert een ingredient dat tijdens een kookworkshop gebruikt
 *          kan worden.
 *
 */
public class Ingredient {

    private String unit;
    private int amountPerPerson;
    private String name;

    /**
     * Initialiseert alle velden met opgegeven waarden.
     *
     * @param name
     *            naam van het ingredient
     * @param amountPerPerson
     *            de hoeveelheid die per persoon gebruikelijk is
     * @param unit
     *            de eenheid (g, kg, l) die van toepassing is op de hoeveelheid
     *            per persoon
     */
    public Ingredient(String name, int amountPerPerson, String unit) {
        this.name = name;
        this.amountPerPerson = amountPerPerson;
        this.unit = unit;
    }

    /**
     * Accessor methode om de eenheid op te vragen die van toepassing is op de
     * hoeveelheid per persoon.
     *
     * @return de eenheid (bv g, kg, l etc.)
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Accessor methode om de de hoeveelheid per persoon van het ingredient op
     * te vragen Gebruik deze in combinatie met de getEenheid methode om deze
     * hoeveelheid te kunnen interpreteren.
     *
     * @return de gebruikelijk hoeveelheid van dit ingredient per persoon
     */
    public int getAmountPerPerson() {
        return amountPerPerson;
    }

    /**
     * Accessor methode om de naam van het ingredient op te vragen
     *
     * @return de naam van het ingredient
     */
    public String getName() {
        return name;
    }
}

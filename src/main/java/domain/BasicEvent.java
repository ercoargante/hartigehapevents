package domain;

import java.util.ArrayList;
import java.util.Date;

/**
 * BasisEvenement model is een super klasse. Demonstratie en Kookworkshop
 * extenden deze klasse.
 * 
 * Hier worden alle parameters en getters en setters gedaan die de subklassen
 * allebij hebben
 * 
 * @author Richard Lindhout
 * @version 1.0
 * 
 * */
public abstract class BasicEvent implements Event {
    private int nrOfParticipants;
    private Date date;
    private int id;
    private String name;

    /**
     * @param id
     *            - id of te event
     * @param name
     *            - name of the event
     * @param date
     *            - date of the event
     * @param nrOfParticipants
     *            - Het aantal mensen wat meedoet met dit evenement
     * @param eventLeader
     *            - Diegene die dit evenement leidt.
     * */
    public BasicEvent(int id, String name, Date date,
            int nrOfParticipants, String eventLeader) {
        super();
        this.id = id;
        this.name = name;
        this.date = date;
        this.nrOfParticipants = nrOfParticipants;
    }

    @Override
    public abstract ArrayList<String> giveEventInfo();

    public int getNrOfParticipants() {
        return nrOfParticipants;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public int getId() {
        return id;
    }
    
    @Override
    public String getName() {
        return name;
    }

}

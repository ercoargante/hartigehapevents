package domain;

import java.util.ArrayList;
import java.util.Date;

/**
 * Deze domein klasse houdt alle extra attributen bij die een demonstratie heeft
 * t.o.v. een basisevenement.
 * 
 * Deze klasse heeft ook ingredienten omdat men wil weten hoeveel ingredienten
 * nodig zijn bij een kookworkshop
 * 
 * */
public class CookingWorkshop extends BasicEvent {
    ArrayList<Ingredient> ingredients;

    /**
     * 
     * @param evenementID
     *            - uniek ID van een evement
     * @param name
     *            - Naam van evenement bijv: Demonstratie van tupperware
     * @param date
     *            - datum waarop demonstratie plaatsvind
     * @param aantaldeelnemers
     *            - hoeveelheid mensen dat meedoet aan demonstratie
     * @param evenemtLeider
     *            - iemand die dit evenement leidt
     * 
     * @param ingredients - Arraylist met ingredienten die bij deze kookworkshop nodig zijn.
     * 
     * */
    public CookingWorkshop(int id, String name, Date date, int nrOfParticipants,
            String eventLeader, ArrayList<Ingredient> ingredients) {

        super(id, name, date, nrOfParticipants, eventLeader);

        this.ingredients = ingredients;
    }

    @Override
    public ArrayList<String> giveEventInfo() {

        ArrayList<String> eventInformation = new ArrayList<String>();
        eventInformation.add(super.getName());
        eventInformation.add("Datum: " + super.getDate());
        eventInformation.add("Naam: " + super.getName());
        eventInformation.add("Aantal deelnemers: " + super.getNrOfParticipants());

        eventInformation.add("\nBenodigede ingredienten: ");

        for (Ingredient ingredient : ingredients) {
            eventInformation.add(ingredient.getName() + " - "
                    + ingredient.getAmountPerPerson() + " "
                    + ingredient.getUnit());

        }

        return eventInformation;
    }
}

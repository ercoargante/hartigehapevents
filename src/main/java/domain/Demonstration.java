package domain;

import java.util.ArrayList;
import java.util.Date;

/**
 * 
 * De demonstratie klasse houdt alle extra attributen bij die een demonstratie heeft
 * t.o.v. een basisevenement (klasse).
 * 
 * @author Richard Lindhout
 * @version 1.0
 * 
 * */
public class Demonstration extends BasicEvent {
    private String contactPerson;
    private String name;
    private String phoneNumber;

    /**
     * 
     * @param evenementID - uniek ID van een evement
     * @param date - datum waarop demonstratie plaatsvind
     * @param aantaldeelnemers - hoeveelheid mensen dat meedoet aan demonstratie
     * @param evenemtLeider - iemand die dit evenement leidt
     * @param name - Naam van evenemnt bijv: Demonstratie van tupperware
     * @param contactPerson - Naam van het contactpersoon bij bedrijf
     * @param phoneNumber - Nummer van het contactpersoon
     * 
     * */
    public Demonstration(int id, Date date, int nrOfParticipsnts,
            String eventLeader, String name, String contactPerson,
            String phoneNumber) {
        super(id, name, date, nrOfParticipsnts, eventLeader);

        this.name = name;
        this.contactPerson = contactPerson;
        this.phoneNumber = phoneNumber;
    }

    
    @Override
    public ArrayList<String> giveEventInfo() {
        ArrayList<String> eventInformation = new ArrayList<String>();
        eventInformation.add(super.getName());
        eventInformation.add("Datum: " + super.getDate());
        eventInformation.add("Persoon (Hartige Hap): " + super.getName());
        eventInformation.add("BedrijfsNaam: " + name);
        eventInformation.add("Contactpersoon: " + contactPerson);
        eventInformation.add("Tel nr: " + phoneNumber);
        eventInformation.add("Aantal deelnemers: " + super.getNrOfParticipants());

        return eventInformation;
    }
}

package domain;

import java.util.ArrayList;
import java.util.Date;

public interface Event {

    /**
     * @return alle informatie die bij een evenement hoort
     * */
    public ArrayList<String> giveEventInfo();

    /**
     * @return Datum object met de datum van het evenement
     * */
    public Date getDate();

    /**
     * @return unieke ID van een evenement
     * */
    public int getId();
    
    /**
     * @return naam van het evenement
     * */
    public String getName();
}

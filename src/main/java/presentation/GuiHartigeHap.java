package presentation;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import domain.Event;
import businesslogic.EventManager;

public class GuiHartigeHap extends JFrame {
    /**
     * The serializable class GUIpanel does not declare a static final
     * serialVersionUID field of type long So we added it.
     */
    private static final long serialVersionUID = 1L;

    EventManager evenementManager;

    public GuiHartigeHap(EventManager evenementManager) {
        this.evenementManager = evenementManager;

        setSize(900, 900);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Hartige Hap Evenementen");

        setContentPane(new GuiPanel(evenementManager));

        setVisible(true);

    }

}

class GuiPanel extends JPanel {
    /**
     * The serializable class GUIpanel does not declare a static final
     * serialVersionUID field of type long So we added it.
     */
    private static final long serialVersionUID = 1L;

    private DefaultTableModel dtm;

    EventManager evenementManager;
    private JTable table;

    private JTextArea textarea;
    private JPanel windows1;
    private JPanel window2;

    public GuiPanel(EventManager evenementManager) {
        this.evenementManager = evenementManager;
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

        //left
        windows1 = new JPanel();
        
        //right
        window2 = new JPanel();

        //make textarea for data input (right)
        textarea = new JTextArea(20, 20);
        
        //make tablecolumns
        String[] columns = new String[] { "ID", "Naam", "Datum"};

        //make table
        table = new JTable();
        table.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        table.setRowHeight(25);
        table.setRowMargin(3);
        table.setRowSelectionAllowed(false);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        
        dtm = new DefaultTableModel(0, 0);
        
        table.setModel(dtm);
        dtm.setColumnIdentifiers(columns);
        
        //insert data 
        for (Event evenement : evenementManager.getEvents()) {
            addRow(evenement.getId(), evenement.getName(), evenement.getDate());
        }

        // Bij muisklik iets doen
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });

        //toevoegengen aan de verschillende vensters
        windows1.add(new JScrollPane(table));
        window2.add(new JScrollPane(textarea));

        //links
        add(windows1);
        
        //rechts
        add(window2);

    }

    public void addRow(int itemID, String naam, Date datum) {
        dtm.addRow(new Object[] { itemID, naam, datum });
    }

    public void resetTableBestelling() {
        dtm.setRowCount(0);
    }

    private void tableMouseClicked(MouseEvent evt) {

        String text = "";
        // clear textarea on next click
        textarea.setText(text);
        int selected = table.getSelectedRow();
        
        // map selected row in the JTable to id of the Event as 
        // known in the manager.
        selected = selected + 1;

        Event evenement = evenementManager
                .getEventInfo(selected);

        for (String string : evenement.giveEventInfo()) {
            text += string;
            text += "\n";
        }

        textarea.setText(text);
    }
}
